package main

import (
	"flag"
	"gitee.com/jinmingzhi/block-chain-news/conf"
	"gitee.com/jinmingzhi/block-chain-news/model"
	"gitee.com/jinmingzhi/block-chain-news/news"
	"gitee.com/jinmingzhi/block-chain-news/notice"
	"github.com/Ankr-network/kit/mlog"
	"time"
)

var (
	file string
	log  = mlog.Logger("block chain news")
)

func init() {
	flag.StringVar(&file, "f", "config.yaml", "default config.yaml")
}

func main() {
	flag.Parse()

	log.Info("init parse file")
	confData := conf.MustParseConfWithFile(file)

	log.Info("init notice")
	notices := notice.NewNotices(confData)

	log.Info("init news")
	news := news.NewNews(time.Second * time.Duration(confData.NewTime))

	log.Info("running...")
	msgChan := make(chan model.Msg, 1)
	news.Handle(msgChan)
	notices.Handle(msgChan)
}
