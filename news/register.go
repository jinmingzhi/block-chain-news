package news

import (
	"gitee.com/jinmingzhi/block-chain-news/model"
	"gitee.com/jinmingzhi/block-chain-news/news/bian"
	"gitee.com/jinmingzhi/block-chain-news/news/gateIo"
	"gitee.com/jinmingzhi/block-chain-news/news/huobi"
	"gitee.com/jinmingzhi/block-chain-news/news/mxc"
	"time"
)

type News []New

func NewNews(t time.Duration) *News {
	n := make(News, 0, 4)
	n = append(n,
		bian.NewBiAn(t),
		huobi.NewHuoBi(t),
		mxc.NewMxc(t),
		gateIo.NewGateIo(t),
	)
	return &n
}

func (n *News) Handle(c chan model.Msg) {
	for _, v := range *n {
		v.Monitor(c)
	}
}
