package news

import "gitee.com/jinmingzhi/block-chain-news/model"

type New interface {
	Monitor(chan<- model.Msg)
}
