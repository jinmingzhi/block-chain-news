module gitee.com/jinmingzhi/block-chain-news

go 1.14

require (
	github.com/Ankr-network/kit v1.8.4
	github.com/antchfx/htmlquery v1.2.3
	github.com/ghodss/yaml v1.0.0
	github.com/stretchr/testify v1.7.0
	go.uber.org/zap v1.15.0
)
