package conf

import (
	"testing"
)

var test_file = `
newTime: 10
notice:
 wx: 
   url: XXXX
   chatRoom: 12131@chatRoom
   uuid: 1111aaa222

 workWx:
   url: AAA
`

func TestParse(t *testing.T) {
	conf := ParseConf([]byte(test_file))
	t.Log(conf)
	t.Log(conf.Notice)
	t.Log(conf.Notice.WorkWx)
	t.Log(conf.Notice.Wx)
}
