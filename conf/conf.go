package conf

import (
	"github.com/ghodss/yaml"
	"go.uber.org/zap"
	"io/ioutil"
)

type Conf struct {
	NewTime int     `json:"newTime"` // 秒数
	Notice  *Notice `json:"notice"`
}

type Notice struct {
	Wx     *Wx     `json:"wx"`
	WorkWx *WorkWx `json:"workWx"`
}

type Wx struct {
	Url      string `json:"url"`
	ChatRoom string `json:"chatRoom"`
	UUId     string `json:"uuid"`
}

type WorkWx struct {
	Url string `json:"url"`
}

func ParseConf(fileBytes []byte) *Conf {
	conf := new(Conf)
	err := yaml.Unmarshal(fileBytes, conf)
	if err != nil {
		log.Fatal("failed to parse conf", zap.Error(err))
	}
	return conf
}

func MustParseConfWithFile(file string) *Conf {
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		log.Fatal("failed to load file", zap.String("file", file), zap.Error(err))
	}
	return ParseConf(bytes)
}
