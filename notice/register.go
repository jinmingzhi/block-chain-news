package notice

import (
	"gitee.com/jinmingzhi/block-chain-news/conf"
	"gitee.com/jinmingzhi/block-chain-news/model"
	"gitee.com/jinmingzhi/block-chain-news/notice/wx_work"
	"go.uber.org/zap"
)

type notices []Notice

func NewNotices(conf *conf.Conf) *notices {
	n := make(notices, 0, 2)
	if w := wx_work.NewWorkWx(conf); w != nil {
		n = append(n, w)
	}

	return &n
}

func (ns *notices) Handle(c <-chan model.Msg) {
	for msg := range c {
		for _, v := range *ns {
			if err := v.Send(msg); err != nil {
				log.Error("failed to notice", zap.String("name", v.GetName()), zap.Error(err))
			}
		}
	}
}
