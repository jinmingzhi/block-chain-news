package notice

import "gitee.com/jinmingzhi/block-chain-news/model"

type Notice interface {
	Send(model.Msg) error
	GetName() string
}
