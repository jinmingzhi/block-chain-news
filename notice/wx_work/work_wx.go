package wx_work

import (
	"fmt"
	"gitee.com/jinmingzhi/block-chain-news/conf"
	"gitee.com/jinmingzhi/block-chain-news/model"
	"net/http"
	"strings"
)

type workWx struct {
	name string
	url  string
}

func NewWorkWx(conf *conf.Conf) *workWx {
	if conf.Notice.WorkWx == nil {
		return nil
	}
	return &workWx{name: "work wx", url: conf.Notice.WorkWx.Url}
}

func (w *workWx) Send(msg model.Msg) error {
	r := strings.NewReader(fmt.Sprintf(`{"msgtype":"text","text":{"content":"%s"}}`, fmt.Sprintf("%s: %s", msg.OriginName, msg.Text)))

	_, err := http.Post(w.url, "application/json", r)
	if err != nil {
		return err
	}
	return nil
}

func (w *workWx) GetName() string {
	return w.name
}
