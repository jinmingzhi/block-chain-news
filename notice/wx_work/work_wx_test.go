package wx_work

import (
	"gitee.com/jinmingzhi/block-chain-news/conf"
	"gitee.com/jinmingzhi/block-chain-news/model"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWorkWx_Send(t *testing.T) {
	w := NewWorkWx(&conf.Conf{Notice: &conf.Notice{WorkWx: &conf.WorkWx{Url: "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=0458320a2dfb28"}}})
	assert.NotNil(t, w)
	err := w.Send(model.Msg{
		OriginName: "test",
		Text:       "test",
	})
	assert.NoError(t, err)
}
