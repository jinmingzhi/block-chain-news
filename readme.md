交易所 公告信息 监控系统
---

### 目前监控的交易所
* 币安
* 火币
* mxc(抹茶)
* gate.io

### config 注解
* 请在conf.yaml 中修改配置信息
```
# 公告请求频次 10s/次(对交易所的公告会每10s检测一次)
newTime: 10

# 通知(目前通知的服务暂且支持企业微信机器人)
notice:
  # 企业微信机器人
  workWx:
    url: http://XXXX     # 这个存放企业微信机器人url
```

### 使用
* 使用前请确保已安装了golang
```
make build
<command> -f conf.yaml 
```
