SHELL = /bin/bash
APP_DIR = "./build"
APP_NAME = block-chain-news

.PHONY:build
build:
	GOOS=windows go build -o ${APP_DIR}/${APP_NAME}-window.exe ./
	GOOS=linux go build -o ${APP_DIR}/${APP_NAME}-linux ./
	GOOS=darwin go build -o ${APP_DIR}/${APP_NAME}-mac ./

